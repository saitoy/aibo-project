//-*-c++-*-
#ifndef INCLUDED_MusicBehavior_h_
#define INCLUDED_MusicBehavior_h_

#include "Behaviors/BehaviorBase.h"
#include "Sound/SoundManager.h"
#include "Shared/ERS210Info.h"
#include "Shared/ERS220Info.h"
#include "Shared/ERS7Info.h"



//! allows you to experiment with playing sounds different ways.
/*! A different sound will be played for each of the buttons, except the head buttons.
 *  When the chin button is held down, any sounds (from this behavior) will be queued
 *  up and then played successively once the chin button is released.
 *
 *  Notice that this doesn't preload all needed sounds:\n
 *  - @c barkmed.wav is listed in ms/config/tekkotsu.cfg as a preloaded system sound
 *  - @c growl.wav will be loaded before being played automatically - notice the
 *    hiccup this can cause.
 */
class MusicBehavior : public BehaviorBase {
public:
	
	//!This is used for referring to sound data so you can start playing it or release it
	typedef unsigned short Snd_ID;
	static const Snd_ID invalid_Snd_ID=(Snd_ID)-1; //!< for reporting errors
	static const Snd_ID MAX_SND=50; //!< the number of sounds that can be loaded at any given time
	
	typedef unsigned short Play_ID;
	static const Play_ID invalid_Play_ID=(Play_ID)-1; //!< for reporting errors
	static const Play_ID MAX_PLAY=256; //!< the number of sounds that can be enqueued at the same time (see MixMode_t)

	Play_ID id;
	

	//! Constructor
	MusicBehavior()
		: BehaviorBase("MusicBehavior"), curplay(SoundManager::invalid_Play_ID), endtime(0),
			LFr(EventBase::buttonEGID,LFrPawOffset,EventBase::activateETID),
			RFr(EventBase::buttonEGID,RFrPawOffset,EventBase::activateETID),
			LBk(EventBase::buttonEGID,LBkPawOffset,EventBase::activateETID),
			RBk(EventBase::buttonEGID,RBkPawOffset,EventBase::activateETID),
			Back(EventBase::buttonEGID,0,EventBase::activateETID)
	{
		
	}
	
	
	//! Load some sounds, listen for button events
	virtual void DoStart() {
		BehaviorBase::DoStart();
		erouter->addListener(this,EventBase::buttonEGID);
		Snd_ID rock = sndman->loadFile("Rock.wav");
		id = sndman->play(rock);
	}

	//! Release sounds we loaded in DoStart()
	virtual void DoStop() {

		BehaviorBase::DoStop();
		sndman->stopPlay(id);
		erouter->removeListener(this);
		sndman->releaseFile("Rock.wav");
	}

	//! Play the sound corresponding to the button
	virtual void processEvent(const EventBase& event) {

			

	}

	//! returns name to system
	static std::string getClassDescription() { return "Plays different sounds when buttons are pressed.  Holding the chin button queues the sounds."; }
	virtual std::string getDescription() const { return getClassDescription(); }

protected:

	static const bool pauseWhileChin=true; //!< if this is true, won't start playing chain until you release the chin button
	SoundManager::Play_ID curplay; //!< current chain (may not be valid if chin button not down or time is past #endtime)
	unsigned int endtime; //!< the expected end of play time for the current chain
	
	//!@name Event Templates
	//!Used to match against the different buttons that have sounds mapped to them
	EventBase LFr,RFr,LBk,RBk,Back;
	//@}
};

	/*! @file
 * @brief Defines the SoundTestBehavior demo, which allows you to experiment with playing sounds different ways.
 * @author ejt (Creator)
 *
 * $Author: ejt $
 * $Name: tekkotsu-3_0 $
 * $Revision: 1.14 $
 * $State: Exp $
 * $Date: 2006/09/18 18:07:57 $
 */

#endif
