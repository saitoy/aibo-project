

using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Drawing;
using System.Threading;
using System.IO;

using NAudio;
using NAudio.Wave;
using NAudio.CoreAudioApi;
using NAudio.Dsp;

namespace AIBOConnect
{
    public class TcpConnector
    {
        /*
         * Each port is designated for particular communications.  There is very little documentation on which
         * ports to use so it would be hard to add any more to this list.
         */
        public const int AIBO_MENU_COMMAND_PORT = 10020;
        public const int AIBO_WALKING_COMMAND_PORT = 10050;
        public const int AIBO_HEADPOINT_COMMAND_PORT = 10052;
        public const int AIBO_TOGGLING_BEHAVIOR_PORT = 10053;
        public const int AIBO_CAMERA_PORT = 10011;
        public const int AIBO_JOINT_MONITOR_PORT = 20002;
        public const int AIBO_COMMUNICATION_PORT = 10001;
        public const int AIBO_MIC_PORT = 10070;     //naudio library

        // /usr/local/Tekkotsu/tools/mon/ControllerGui

        /* 
         * Each of the following is a command that can be sent to the AIBO.  They are menu commands, so as an example:
         * "!root \"Status Reports\" \"Battery Check\"\n" tells the AIBO to go to the root menu, then go down a sub-level
         * to the Satus Reports menu, then goes down a level to the Battery Check menu.  When the AIBO receives these commands,
         * it will start responding with information reguarding that menu.
         * 
         * Important: If you are looking for more commands/menus that the AIBO has since there is no documentation elsewhere
         * that I can find, go to the "project" folder that you should have used to copy over to the AIBO's memory stick.  
         * Within it you should see some classes such as:
         * StartupBehavior.cc, StartupBehavior_SetupStatusReports.cc, StartupBehavior_SetupModeSwitch.cc, etc
         * Each of these classes contain information on their menus that are available.  Study these and you can
         * create your own menu commands by using the same structure shown in the following strings.
         */
        public const string TEKKOTSUMON_GETUP_CONTROL = "!root \"Background Behaviors\" \"System Daemons\" \"Auto Getup\"\n";
        public const string TEKKOTSUMON_BATTERY_CHECK = "!root \"Status Reports\" \"Battery Check\"\n";
        public const string TEKKOTSUMON_JOINT_MONITOR = "!root \"Mode Switch\" \"Joint\"\n";
        public const string TEKKOTSUMON_WALK_REMOTE_CONTROL = "!root \"TekkotsuMon\" \"Walk Remote Control\"\n";
        public const string TEKKOTSUMON_POWERWALK_CONTROL = "!root \"TekkotsuMon\" \"UPenn Walk Remote Control\"\n";
        public const string TEKKOTSUMON_HEAD_REMOTE_CONTROL = "!root \"TekkotsuMon\" \"Head Remote Control\"\n";
        public const string CMDToggleVision = "!root \"TekkotsuMon\" \"Raw Cam Server\"\r\n";
        public const string CMDToggleAudio = "!root \"TekkotsuMon\" \"Microphone Server\"\r\n";
        public const string HELLOWORLD = "!root \"Background Behaviors\" \"HelloWorldBehavior\"\r\n";
        public const string DANCE = "!root \"File Access\" \"Run Motion Sequence\" \"dance.mot\"\n";
        public const string PUNCH = "!root \"File Access\" \"Run Motion Sequence\" \"k_punch.mot\"\n";
        public const string GRAB = "!root \"File Access\" \"Run Motion Sequence\" \"k_grab.mot\"\n";
        public const string FLASHIP = "!root \"Background Behaviors\" \"System Daemons\" \"FlashIPAddrBehavior\"\n";
        public const string GETUP = "!root \"Background Behaviors\" \"AutoGetupBehavior2\"\n";
        public const string WAGTAIL = "!root \"Background Behaviors\" \"Wag Tail\"\n";
        public const string MUSIC = "!root \"Background Behaviors\" \"MusicBehavior\"\n";
        public const string SIT = "!root \"File Access\" \"Run Motion Sequence\" \"k-stsit.mot\"\n";
        public const string ROAR = "!root \"File Access\" \"Run Motion Sequence\" \"k_head.mot\"\n";
        public const string LIEDOWN = "!root \"File Access\" \"Run Motion Sequence\" \"liedown.mot\"\n";
        public const string SPRAWL = "!root \"File Access\" \"Run Motion Sequence\" \"sprawl.mot\"\n";
        public const string CROUCH = "!root \"File Access\" \"Run Motion Sequence\" \"crouch.mot\"\n";
        public const string POUNCE = "!root \"File Access\" \"Run Motion Sequence\" \"pounce.mot\"\n";
        public const string RELAX = "!root \"Background Behaviors\" \"Relax\"\n";
        public const string ALEAVEIT = "!root \"File Access\" \"Play Sound\" \"ALeave~1.wav\"\n";
        public const string ADANGEROUS = "!root \"File Access\" \"Play Sound\" \"ASITUA~1.wav\"\n";
        public const string ASTOPIT = "!root \"File Access\" \"Play Sound\" \"AStopIt.wav\"\n";
        public const string HBLAME = "!root \"File Access\" \"Play Sound\" \"HIDont~1.wav\"\n";
        public const string HNOHARDFEELINGS = "!root \"File Access\" \"Play Sound\" \"HNOHAR~1.wav\"\n";
        public const string SCOMINGBACK = "!root \"File Access\" \"Play Sound\" \"SAreYo~1.wav\"\n";
        public const string SSTILLTHERE = "!root \"File Access\" \"Play Sound\" \"SAreYo~2.wav\"\n";
        public const string SPUTMEDOWN = "!root \"File Access\" \"Play Sound\" \"SPutMe~1.wav\"\n";
        public const string SSTOP = "!root \"File Access\" \"Play Sound\" \"SStop.wav\"\n";

        // These are some predefined characters that the AIBO understands what to do when they receive these (on the proper port).
        // If there are any more, it is difficult to find them.
        public const char CMD_WALK_FORWARD = 'f';
        public const char CMD_WALK_ROTATE = 'r';
        public const char CMD_WALK_STRAFE = 's';

        public const char CMD_HEADPOINT_TILT = 't';
        public const char CMD_HEADPOINT_ROLL = 'r';
        public const char CMD_HEADPOINT_PAN = 'p';

        // Set the HOST_IP to the IP address of the AIBO prior to trying to connect
        public string HOST_IP {get;set;}
        private const int MAX_TRIES = 5;

        private bool _allConnected, _micConnected, _tcpConnected, _powerWalkConnected, _walkConnected, _headConnected, _behaviorConnected, _jointMonitorConnected, _communicationConnected;
        public bool TcpConnected { 
            get { return _tcpConnected; }
            set { 
                _tcpConnected = value;
                SendCommunicationStatus("TCP", value);
            }
        }
        public bool CommunicationConnected
        {
            get { return _communicationConnected; }
            set
            {
                _communicationConnected = value;
                SendCommunicationStatus("Comm", value);
            }
        }
        public bool WalkConnected
        {
            get { return _walkConnected; }
            set {
                _walkConnected = value;
                SendCommunicationStatus("Walk", value);
            }
        }
        public bool PowerWalkConnected
        {
            get { return _powerWalkConnected; }
            set
            {
                _powerWalkConnected = value;
                SendCommunicationStatus("PWalk", value);
            }
        }
        public bool HeadConnected
        {
            get { return _headConnected; }
            set
            {
                _headConnected = value;
                SendCommunicationStatus("Head", value);
            }
        }
        public bool BehaviorConnected
        {
            get { return _behaviorConnected; }
            set
            {
                _behaviorConnected = value;
                SendCommunicationStatus("Behavior", value);
            }
        }
        public bool JointMonitorConnected
        {
            get { return _jointMonitorConnected; }
            set
            {
                _jointMonitorConnected = value;
                SendCommunicationStatus("Joint Monitor", value);
            }
        }
        public bool micConnected
        {
            get { return _micConnected; }
            set
            {
                _micConnected = value;
                SendCommunicationStatus("Microphone", value);
            }
        }
        public bool allConnected
        {
            get { return _micConnected; }
            set
            {
                _allConnected = value;
                SendCommunicationStatus("All Systems", value);
            }
        }

        private TcpClient micClient, menuCmdClient, walkCmdClient, headCmdClient, behaviorCmdClient, jointMonitorClient, communicationClient;
        private UdpClient UDPCamera;
        private IPEndPoint cameraRP;
        private NetworkStream batteryStream, micStream, sendCmdStream, sendWalkCmdStream, sendHeadCmdStream, sendBehaviorCmdStream, jointMonitorStream;
        private Thread connector;
        private Thread cameraClient, audioClient, battery;
        public String batteryInfo;
        
        private Image currImage;

        public delegate void CommunicationEventCallback(string component, bool connected);
        public event CommunicationEventCallback CommunicationStatusChanged;
        private void SendCommunicationStatus(string component, bool connected)
        {
            if (CommunicationStatusChanged != null)
            {
                CommunicationStatusChanged(component, connected);
            }
        }

        public TcpConnector()
        {
            _tcpConnected = false;
            _walkConnected = false;
            _headConnected = false;
            _behaviorConnected = false;
            _communicationConnected = false;
            _micConnected = false;
            _allConnected = false;

            micClient = null;
            menuCmdClient = null;
            headCmdClient = null;
            walkCmdClient = null;
            behaviorCmdClient = null;
            communicationClient = null;
        }

        // Using CurrImage will give you a picture of what the AIBO was looking at.  Using this repeatedly can create a video feed.
        public Image CurrImage
        {
            get
            {
                return currImage;
            }
        }

        // Starts a connection with the AIBO when called.  Be sure to first set the AIBO's HOST_IP before trying to connect
        public void ConnectToAIBO()
        {
            connector = new Thread(new ThreadStart(Run));
            connector.Start();
        }

        private void Run()
        {
            InitCommunication();
            InitBehavior();
            InitRemoteWalkControl(false);
            InitRemoteHeadControl();
            InitJointMonitor();
            InitCommRelay();
            InitMic();

            if (TcpConnected)
            {
                cameraClient = new Thread(new ThreadStart(RemoteCameraThread));
                cameraClient.Start();

                audioClient = new Thread(new ThreadStart(getAudioData));
                audioClient.Start();

                battery = new Thread(new ThreadStart(batteryThread));
                battery.Start();
            }

            SendHeadPointCmd(CMD_HEADPOINT_PAN, 0f);
            SendHeadPointCmd(CMD_HEADPOINT_ROLL, 0f);
            SendHeadPointCmd(CMD_HEADPOINT_TILT, 0.0f);

            if (TcpConnected & CommunicationConnected & WalkConnected & HeadConnected & micConnected & BehaviorConnected)
                allConnected = true;

            SendCommand(POUNCE);
            SendHeadPointCmd('3', 0.0f);
        }
        
        // Initializes the communication port connection that will be used for gather information like Battery Level.
        // This communicationClient likely could also be used for requesting sensor information like the paws.
        public void InitCommRelay()
        {
            int tries = 0;

            while ((tries < MAX_TRIES) && !CommunicationConnected)
            {
                try
                {
                    string dots = null;
                    for (int i = 0; i < tries + 1; i++)
                    {
                        dots += ".";
                    }

                    string msg = "Connecting to port: " + AIBO_COMMUNICATION_PORT + " (Communication)" + dots;

                    communicationClient = new TcpClient(HOST_IP, AIBO_COMMUNICATION_PORT);

                    msg = "Port: " + AIBO_COMMUNICATION_PORT + " (Communication) - Connected";

                    sendCmdStream = communicationClient.GetStream();
                    CommunicationConnected = true;
                    Thread.Sleep(300);
                }
                catch (Exception e)
                {
                    CommunicationConnected = false;
                    Console.WriteLine(e.ToString());
                }

                tries += 1;
            }
        }

        public void InitCommunication()
        {
            int tries = 0;

            while ((tries < MAX_TRIES) && !TcpConnected)
            {
                try
                {
                    string dots = null;
                    for (int i = 0; i < tries + 1; i++)
                    {
                        dots += ".";
                    }

                    string msg = "Connecting to port: " + AIBO_MENU_COMMAND_PORT + " (Menu)" + dots;

                    menuCmdClient = new TcpClient(HOST_IP, AIBO_MENU_COMMAND_PORT);

                    msg = "Port: " + AIBO_MENU_COMMAND_PORT + " (Menu) - Connected";

                    sendCmdStream = menuCmdClient.GetStream();
                    TcpConnected = true;
                    Thread.Sleep(300);
                }
                catch (Exception e)
                {
                    TcpConnected = false;
                    Console.WriteLine(e.ToString());
                }

                tries += 1;
            }
        }

        // Call this function to send a command to the AIBO to automatically get up.  The AIBO has the
        // tendency to hit its power button while attempting this so don't be alarmed if it turns off right
        // after completing this action.
        public void resurrectAIBO()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(TEKKOTSUMON_GETUP_CONTROL);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void powerWalk()
        {
            SendCommand(TEKKOTSUMON_WALK_REMOTE_CONTROL);
            SendCommand(TEKKOTSUMON_POWERWALK_CONTROL);
        }

        public void Music()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(MUSIC);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Punch()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(PUNCH);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Getup()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(GETUP);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void HelloWorld()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(HELLOWORLD);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void FlashIP()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(FLASHIP);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Sprawl()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(SPRAWL);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Dance()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(DANCE);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Sit()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(SIT);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Grab()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(GRAB);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Roar()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(ROAR);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Liedown()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(LIEDOWN);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Crouch()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(CROUCH);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Wagtail()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(WAGTAIL);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Pounce()
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(POUNCE);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }

        public void Dialogue(string mood, string direction)
        {
            if (mood.Equals("Happy") && direction.Equals("Up"))
            {
                Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(HBLAME);
                sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                sendCmdStream.Flush();
            }
            else if (mood.Equals("Happy") && direction.Equals("Down"))
            {
                Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(HNOHARDFEELINGS);
                sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                sendCmdStream.Flush();
            }
            if(mood.Equals("Angry") && direction.Equals("Up"))
            {
                Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(ALEAVEIT);
                sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                sendCmdStream.Flush();
            }
            else if (mood.Equals("Angry") && direction.Equals("Down"))
            {
                Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(ASTOPIT);
                sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                sendCmdStream.Flush();
            }
            else if (mood.Equals("Angry") && direction.Equals("Right"))
            {
                Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(ADANGEROUS);
                sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                sendCmdStream.Flush();
            }
            else if (mood.Equals("Sad") && direction.Equals("Right"))
            {
                Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(SPUTMEDOWN);
                sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                sendCmdStream.Flush();
            }
            else if (mood.Equals("Sad") && direction.Equals("Left"))
            {
                Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(SSTOP);
                sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                sendCmdStream.Flush();
            }
            else if (mood.Equals("Sad") && direction.Equals("Up"))
            {
                Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(SCOMINGBACK);
                sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                sendCmdStream.Flush();
            }
            else if (mood.Equals("Sad") && direction.Equals("Down"))
            {
                Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(SSTILLTHERE);
                sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                sendCmdStream.Flush();
            }
        }

        // This function does not work completely right.  Sometimes the AIBO responds with a lot of information and
        // sometimes it doesn't.  Calling this function might cause your program to freeze.  I recommend trying to rewrite it.
        // This is purely to to try and give an understanding of how communicating with the AIBO can potentially work.
        public int getPowerRemaining()
        {
            if (!CommunicationConnected)
            {
                //Console.WriteLine("Cannot request battery information, The Comms Relay is not connected.");
            }
            else
            {

                batteryStream = menuCmdClient.GetStream();

                // Changes the menu to the battery check menu
                Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(TEKKOTSUMON_BATTERY_CHECK);
                batteryStream.Write(sendBytes, 0, sendBytes.Length);
                batteryStream.Flush();

                
                // Uses the communicationClient (port 10001) in order to receive the output from the AIBO.
                // It should be giving you information similar to if you were to telnet to the AIBO on port 59000.
                batteryStream = communicationClient.GetStream();
                Byte[] rcvBytes = new Byte[75];
                string response = "";


                
                // while the AIBO is still sending information, keep reading the information and concatenating it to a string
                // The AIBO might not stop sending information so it might be possible this is an infinite loop.

                if (batteryStream.Read(rcvBytes, 0, rcvBytes.Length) != 0)
                {
                    response = System.Text.Encoding.ASCII.GetString(rcvBytes);
                    response = response.Trim();
                    
                }

                Thread.Sleep(500);
                //while (sendCmdStream.Read(rcvBytes, 0, rcvBytes.Length) != 0)
                //{
                //    sendCmdStream.Flush();
                //    response = response + System.Text.Encoding.ASCII.GetString(rcvBytes);
                //    Console.WriteLine("HELLO!");
                //}

                if (response.Contains("Power") && (response[0].Equals("  ") == false))
                {
                    batteryInfo = response;
                }
      

            }
            
            // You would do some string parsing here to find the power remaining, then return it as the result.
            

            return -1;
        }

        public void getAudioData()
        {
            byte[] recData = new Byte[4];

            WaveFormat waveFormat;
            WaveBuffer waveBuffer;
            BufferedWaveProvider waveProvider;
            DirectSoundOut waveOut;
            //WaveOut waveOut;
            BiQuadFilter bqf;
            //float sample;

            waveFormat = new WaveFormat(16000, 16, 2);
            waveOut = new DirectSoundOut(300);
            waveProvider = new BufferedWaveProvider(waveFormat);
            waveOut.Init(waveProvider);
            //WaveFileWriter a = new WaveFileWriter();

            bqf = BiQuadFilter.LowPassFilter(16000, 10000, 1);

            if (!micConnected)
            {
                Console.WriteLine("Cannot request audio information, The mic is not connected.");
            }
            else
            {
                waveBuffer = new WaveBuffer(recData);
                while (micStream.Read(recData, 0, recData.Length) != 0){

                    //for (int n = 0; n < recData.Length; n += 2)
                    //{
                    //    sample = BitConverter.ToSingle(recData, n);
                    //}

                    //float biggest = 0F;
                    //float sample;
                    //for (int n = 0; n < waveBuffer.FloatBuffer.Length; n++)
                    //{
                    //    sample = waveBuffer.FloatBuffer[n];
                    //    if (Math.Abs(sample) > biggest) biggest = sample;
                    //}

                    //biggest /= 32768F;

                    //for (int n = 0; n < waveBuffer.FloatBuffer.Length; n++)
                    //{
                    //    waveBuffer.FloatBuffer[n] = waveBuffer.FloatBuffer[n] * (1F / biggest);
                    //}

                    waveProvider.AddSamples(recData, 0, recData.Length);
                    if (waveProvider.BufferedBytes >= 319000)
                    {
                        while (waveOut.PlaybackState != PlaybackState.Stopped)
                            Thread.Sleep(100);
                        //waveOut.Play();
                        waveProvider.ClearBuffer();
                        
                    }
                    micStream.Flush();
                }
            }
        }

        short GetShortFromLittleEndianBytes(byte[] data, int startIndex)
        {
            return (short)((data[startIndex + 1] << 8)
                 | data[startIndex]);
        }

        byte[] GetLittleEndianBytesFromShort(short data)
        {
            byte[] b = new byte[2];
            b[0] = (byte)data;
            b[1] = (byte)(data >> 8 & 0xFF);
            return b;
        }

        byte[] normalizeAudio(byte[] input)
        {
            WaveBuffer waveBuffer = new WaveBuffer(input);

            float biggest = 0F;
            float sample;
            for (int n = 0; n < waveBuffer.FloatBuffer.Length; n ++)
            {
                sample = waveBuffer.FloatBuffer[n];
                if (Math.Abs(sample) > biggest) biggest = sample;
            }

            biggest /= 32768F;

            float[] data = new float[input.Length / 2];

            for (int n = 0; n < waveBuffer.FloatBuffer.Length; n ++)
            {
                waveBuffer.FloatBuffer[n] = waveBuffer.FloatBuffer[n] * (1F / biggest);
            }

            byte[] output = new byte[input.Length];
            for (int i = 0; i < output.Length; i += 2)
            {
                byte[] tmp = GetLittleEndianBytesFromShort(Convert.ToInt16(data[i / 2]));
                output[i] = tmp[0];
                output[i + 1] = tmp[1];
            }
            return output;
        }

        public void InitPowerWalkControl()
        {
            int tries = 0;

            if (TcpConnected)
            {
                while ((tries < MAX_TRIES) && !PowerWalkConnected)
                {
                    try
                    {
                        Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(TEKKOTSUMON_POWERWALK_CONTROL);
                        sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                        sendCmdStream.Flush();

                        //string dots = null;
                        //for (int i = 0; i < tries + 1; i++)
                        //{
                        //    dots += ".";
                        //}

                        //string msg = "Connecting to port: " + AIBO_WALKING_COMMAND_PORT + " (Walk)" + dots;

                        //walkCmdClient = new TcpClient(HOST_IP, AIBO_WALKING_COMMAND_PORT);

                        //msg = "Port: " + AIBO_WALKING_COMMAND_PORT + " (Walk) - Connected";

                        //sendWalkCmdStream = walkCmdClient.GetStream();
                        PowerWalkConnected = true;
                        Thread.Sleep(300);
                    }
                    catch (Exception e)
                    {
                        PowerWalkConnected = false;
                        Console.WriteLine(e.ToString());
                    }

                    tries += 1;
                }
            }
            else
            {
                PowerWalkConnected = false;
            }
        }

        public void InitRemoteWalkControl(bool power)
        {
            int tries = 0;
            bool init = power;

            if (TcpConnected)
            {
                while (((tries < MAX_TRIES) && !WalkConnected) || init)
                {
                    try
                    {
                        if (init)
                        {
                            SendCommand(TEKKOTSUMON_POWERWALK_CONTROL);
                            init = false;
                        }
                        else
                            SendCommand(TEKKOTSUMON_WALK_REMOTE_CONTROL);

                        string dots = null;
                        for (int i = 0; i < tries + 1; i++)
                        {
                            dots += ".";
                        }

                        string msg = "Connecting to port: " + AIBO_WALKING_COMMAND_PORT + " (Walk)" + dots;

                        walkCmdClient = new TcpClient(HOST_IP, AIBO_WALKING_COMMAND_PORT);

                        msg = "Port: " + AIBO_WALKING_COMMAND_PORT + " (Walk) - Connected";

                        sendWalkCmdStream = walkCmdClient.GetStream();
                        WalkConnected = true;
                        Thread.Sleep(300);
                    }
                    catch (Exception e)
                    {
                        WalkConnected = false;
                        Console.WriteLine(e.ToString());
                    }

                    tries += 1;
                }
            }
            else
            {
                WalkConnected = false;
            }
        }

        public void InitRemoteHeadControl()
        {
            int tries = 0;

            if (TcpConnected)
            {
                while ((tries < MAX_TRIES) && !HeadConnected)
                {
                    try
                    {
                        Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(TEKKOTSUMON_HEAD_REMOTE_CONTROL);
                        sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                        sendCmdStream.Flush();

                        string dots = null;
                        for (int i = 0; i < tries + 1; i++)
                        {
                            dots += ".";
                        }

                        string msg = "Connecting to port: " + AIBO_HEADPOINT_COMMAND_PORT + " (Head)" + dots;

                        headCmdClient = new TcpClient(HOST_IP, AIBO_HEADPOINT_COMMAND_PORT);

                        msg = "Port: " + AIBO_HEADPOINT_COMMAND_PORT + " (Head) - Connected";

                        sendHeadCmdStream = headCmdClient.GetStream();
                        HeadConnected = true;
                        Thread.Sleep(300);
                    }
                    catch (Exception e)
                    {
                        HeadConnected = false;
                        Console.WriteLine(e.ToString());
                    }

                    tries += 1;
                }
            }
            else
            {
                HeadConnected = false;
            }
        }

        public void InitBehavior()
        {
            int tries = 0;

            if (TcpConnected)
            {
                while ((tries < MAX_TRIES) && !BehaviorConnected)
                {
                    try
                    {
                        string msg = "Connecting to port: " + AIBO_TOGGLING_BEHAVIOR_PORT + " (Behavior)...";

                        behaviorCmdClient = new TcpClient(HOST_IP, AIBO_TOGGLING_BEHAVIOR_PORT);

                        msg = "Port: " + AIBO_TOGGLING_BEHAVIOR_PORT + " (Behavior) - Connected";

                        sendBehaviorCmdStream = behaviorCmdClient.GetStream();
                        BehaviorConnected = true;
                        this.StartBehavior();
                        Thread.Sleep(500);
                    }
                    catch (Exception e)
                    {
                        BehaviorConnected = false;
                        Console.WriteLine(e.ToString());
                    }

                    tries += 1;
                }
            }
            else
            {
                BehaviorConnected = false;
            }
        }

        public void InitJointMonitor()
        {
            int tries = 0;

            if (TcpConnected)
            {
                while ((tries < MAX_TRIES) && !JointMonitorConnected)
                {
                    try
                    {
                        Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(TEKKOTSUMON_JOINT_MONITOR);
                        sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                        sendCmdStream.Flush();

                        jointMonitorClient = new TcpClient(HOST_IP, AIBO_JOINT_MONITOR_PORT);


                        jointMonitorStream = jointMonitorClient.GetStream();
                        JointMonitorConnected = true;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }

                    tries += 1;
                }
            }
            else
            {
                JointMonitorConnected = false;
            }
            
        }

        public void InitMic()
        {
            int tries = 0;
            SendCommand(CMDToggleAudio);
            Thread.Sleep(500);

            while ((tries < MAX_TRIES) && !micConnected)
                try
                {
                    string dots = null;
                    for (int i = 0; i < tries + 1; i++)
                    {
                        string msg = "Connecting to port: " + AIBO_MIC_PORT + " (Mic)" + dots;

                        micClient = new TcpClient(HOST_IP, AIBO_MIC_PORT);

                        msg = "Port: " + AIBO_MIC_PORT + " (Mic) - Connected";

                        micStream = micClient.GetStream();
                        micConnected = true;
                        Thread.Sleep(300);
                    }
                }
                catch (Exception e)
                {
                    micConnected = false;
                    Console.WriteLine(e.ToString());
                }
        }

        public void SendCommand(string command)
        {
            Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(command);
            sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
            sendCmdStream.Flush();
        }



        private void RemoteCameraThread()
        {
            SendCommand(CMDToggleVision);
            Thread.Sleep(500);
            
            cameraRP = new IPEndPoint(IPAddress.Parse(HOST_IP), AIBO_CAMERA_PORT);
            UDPCamera = new UdpClient(HOST_IP, AIBO_CAMERA_PORT);

            Byte[] dgram = System.Text.Encoding.ASCII.GetBytes("connection request");
            UDPCamera.Send(dgram, dgram.Length);

            System.Text.ASCIIEncoding encode = new System.Text.ASCIIEncoding();
            while (UDPCamera != null)
            {
                try
                {
                    byte[] recData = UDPCamera.Receive(ref cameraRP);
                    ParsePacket(recData);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }

        private void batteryThread()
        {
            while (true)
            {
                getPowerRemaining();
                Thread.Sleep(15000);
            }

        }

        // Use this function along with a walk type (like 'f' for forward) and an amount between -1.0 and 1.0
        // in order to make the AIBO move.  Note that if you walk forward, then go to strafe sideways as an example, if you do
        // not send a 0.0 value for walking prior to strafing, it will still remember the value you sent it to walk forward.
        // (Meaning that it will combine strafing and walking forward).
        public void SendWalkCmd(char WalkType, float amount)
        {
            Console.WriteLine("SendWalkCmd is Called");
            if (true)//sendWalkCmdStream != null)
            {
                Byte[] sendBytes = new Byte[5];
                sendBytes[0] = (byte)WalkType;
                Byte[] floatB = System.BitConverter.GetBytes(amount);

                for (int i = 0; i < floatB.Length; i++)
                {
                    Console.WriteLine("float " + i + ": " + floatB[i]);
                    sendBytes[i + 1] = floatB[i];
                }

                try
                {
                    sendWalkCmdStream.Write(sendBytes, 0, sendBytes.Length);
                    sendWalkCmdStream.Flush();
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.StackTrace);
                }
            }
        }

        // Similar to the Walk command (see comments) except that this is purely head movement.
        public void SendHeadPointCmd(char HPType, float amount)
        {
            if (sendHeadCmdStream != null)
            {
                Byte[] sendBytes = new Byte[5];
                sendBytes[0] = (byte)HPType;
                Byte[] floatB = System.BitConverter.GetBytes(amount);

                for (int i = 0; i < floatB.Length; i++)
                {
                    sendBytes[i + 1] = floatB[i];
                }

                try
                {
                    sendHeadCmdStream.Write(sendBytes, 0, sendBytes.Length);
                    sendHeadCmdStream.Flush();
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.StackTrace);
                }
            }
        }

        public void StartBehavior()
        {
            if (sendBehaviorCmdStream != null)
            {
                try
                {
                    string msg = "start\n";
                    Byte[] sendBytes = Encoding.ASCII.GetBytes(msg);
                    sendBehaviorCmdStream.Write(sendBytes, 0, sendBytes.Length);
                    sendBehaviorCmdStream.Flush();
                    Console.WriteLine("Behavior Started");
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.StackTrace);
                }
            }
            else
            {
                Console.WriteLine("Wiibot.TcpConnector.StartBehavior(): Behavior Stream is NULL");
            }
        }

        public void WalkForward()
        {
            if (jointMonitorStream != null)
            {
                try
                {
                    string msg = "FWD";
                    Byte[] sendBytes = Encoding.ASCII.GetBytes(msg);
                    jointMonitorStream.Write(sendBytes, 0, sendBytes.Length);
                    jointMonitorStream.Flush();
                    Console.WriteLine("Walking forward");
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.StackTrace);
                }
            }
            else
            {
                Console.WriteLine("Wiibot.TcpConnector.StartBehavior(): Behavior Stream is NULL");
            }
        }

        public void StopForward()
        {
            if (jointMonitorStream != null)
            {
                try
                {
                    string msg = "STP";
                    Byte[] sendBytes = Encoding.ASCII.GetBytes(msg);
                    jointMonitorStream.Write(sendBytes, 0, sendBytes.Length);
                    jointMonitorStream.Flush();
                    Console.WriteLine("Stop forward");
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.StackTrace);
                }
            }
            else
            {
                Console.WriteLine("Wiibot.TcpConnector.StartBehavior(): Behavior Stream is NULL");
            }
        }

        public void StopBehavior()
        {
            if (sendBehaviorCmdStream != null)
            {
                try
                {
                    string msg = "stop\n";
                    Byte[] sendBytes = Encoding.ASCII.GetBytes(msg);
                    sendBehaviorCmdStream.Write(sendBytes, 0, sendBytes.Length);
                    sendBehaviorCmdStream.Flush();
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.StackTrace);
                }
            }
            else
            {
                Console.WriteLine("Wiibot.TcpConnector.StopBehavior(): Behavior Stream is NULL");
            }
        }

        public void CloseConnection()
        {
            try
            {
                if (sendCmdStream != null)
                {
                    battery.Abort();
                    if (WalkConnected)
                    {
                        Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(TEKKOTSUMON_WALK_REMOTE_CONTROL);
                        sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                        sendCmdStream.Flush();

                        if (sendWalkCmdStream != null)
                        {
                            sendWalkCmdStream.Close();
                            sendWalkCmdStream = null;
                            walkCmdClient.Close();
                            WalkConnected = false;
                        }
                        Console.WriteLine("End walk");
                        Thread.Sleep(300);
                    }

                    if (HeadConnected)
                    {
                        Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(TEKKOTSUMON_HEAD_REMOTE_CONTROL);
                        sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                        sendCmdStream.Flush();

                        if (sendHeadCmdStream != null)
                        {
                            sendHeadCmdStream.Close();
                            sendHeadCmdStream = null;
                            headCmdClient.Close();
                            HeadConnected = false;

                        }
                        Console.WriteLine("End head");
                        Thread.Sleep(300);
                    }

                    if (BehaviorConnected)
                    {
                        sendBehaviorCmdStream.Close();
                        sendBehaviorCmdStream = null;
                        behaviorCmdClient.Close();
                        BehaviorConnected = false;
                        Thread.Sleep(300);
                    }
                    if (JointMonitorConnected)
                    {
                        Byte[] sendBytes = System.Text.Encoding.ASCII.GetBytes(TEKKOTSUMON_JOINT_MONITOR);
                        sendCmdStream.Write(sendBytes, 0, sendBytes.Length);
                        sendCmdStream.Flush();

                        Thread.Sleep(300);

                        jointMonitorStream.Close();
                        jointMonitorStream = null;
                        jointMonitorClient.Close();
                        JointMonitorConnected = false;
                    }

                    if (micConnected)
                    {
                        audioClient.Abort();
                        SendCommand(CMDToggleAudio);
                        Thread.Sleep(300);

                        micStream.Close();
                        micStream = null;
                        micClient.Close();
                        micConnected = false;
                    }


                    if (UDPCamera != null)
                    {
                        SendCommand(CMDToggleVision);
                        cameraClient.Abort();
                        UDPCamera.Close();
                    }

                    sendCmdStream.Close();
                    sendCmdStream = null;

                    connector.Abort();
                    connector = null;

                    TcpConnected = false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        #region Camera Parsing Code
        uint getInt(byte[] input, int index)
        {
            return ((uint)(
                (byte)input[0 + index] |
                (byte)input[1 + index] << 8 |
                (byte)input[2 + index] << 16 |
                (byte)input[3 + index] << 24
            ));
        }

        class AIBOPacket
        {

            public uint nameLen;
            public string name;
            public uint imageEncoding;
            public uint imageCompression;
            public uint imageWidth;
            public uint imageHeight;
            public uint imageTimestamp;
            public uint imageFramenumber;

            // Generator info
            public uint generatorNameLen;
            public string generatorName;
            public uint generatorImageWidth;
            public uint generatorImageHeight;
            public uint generatorImageLayer;
            public uint generatorImageChannel;

            public uint specificGenNameLen;
            public string specificGenName;
            public uint jpegImageDataSize;

        }

        void ParsePacket(byte[] packet)
        {
            int index = 0;
            AIBOPacket ap = new AIBOPacket();

            ap.nameLen = getInt(packet, index); index += 4;
            ap.name = System.Text.ASCIIEncoding.ASCII.GetString(packet, index, (int)ap.nameLen);
            index += (int)ap.nameLen + 1;
            ap.imageEncoding = getInt(packet, index); index += 4;
            ap.imageCompression = getInt(packet, index); index += 4;
            ap.imageWidth = getInt(packet, index); index += 4;
            ap.imageHeight = getInt(packet, index); index += 4;
            ap.imageTimestamp = getInt(packet, index); index += 4;
            ap.imageFramenumber = getInt(packet, index); index += 4;

            ap.generatorNameLen = getInt(packet, index); index += 4;
            ap.generatorName = System.Text.ASCIIEncoding.ASCII.GetString(packet, index,
                                (int)ap.generatorNameLen);
            index += (int)ap.generatorNameLen + 1;

            ap.generatorImageWidth = getInt(packet, index); index += 4;
            ap.generatorImageHeight = getInt(packet, index); index += 4;
            ap.generatorImageLayer = getInt(packet, index); index += 4;
            ap.generatorImageChannel = getInt(packet, index); index += 4;

            ap.specificGenNameLen = getInt(packet, index); index += 4;
            ap.specificGenName = System.Text.ASCIIEncoding.ASCII.GetString(packet, index,
                                   (int)ap.specificGenNameLen);
            index += (int)ap.specificGenNameLen + 1;

            ap.jpegImageDataSize = getInt(packet, index); index += 4;

            System.IO.MemoryStream str = new System.IO.MemoryStream(packet, index, packet.Length - index, true, true);

            this.currImage = Image.FromStream(str, false, false);
            //			UpdateImage(Image.FromStream(str,false,false));
        }
        #endregion
    }

}