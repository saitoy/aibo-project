﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Timers;
using System.Media;

using System.Windows.Shapes;
using System.Threading;
using AIBOConnect;
using SharpDX;
using SharpDX.XInput;

using System.Windows.Threading;
using System.IO;

namespace AiboTutorial
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        TcpConnector AiboConnection = new TcpConnector();
        DispatcherTimer updateImageTimer = new DispatcherTimer();
        DispatcherTimer updateControllerTimer = new DispatcherTimer();
        DispatcherTimer batteryTimer = new DispatcherTimer();

        Controller controller = new Controller(UserIndex.One);
        State controllerState;
        bool legMoving = false;
        bool pWalk = false;
        bool musictoggle = false;
        bool lookingBack = false;
        bool needGetup = false;
        bool needResetTail = false;
        //State previousState;
        System.Timers.Timer timer;
        System.Timers.Timer timer2;
        System.Timers.Timer childtimer;
        System.Timers.Timer childmessagetimer;

        String Happy1 = "I don't blame you";
        String Happy2 = "No hard feelings";
        String Angry1 = "Leave it";
        String Angry2 = "Stop it";
        String Angry3 = "Danger";
        String Sad1 = "Coming Back";
        String Sad2 = "Still There";
        String Sad3 = "Put me down";
        String Sad4 = "Please Stop";

        int DEADZONE = 5000;
        int TRIGGER_DEADZONE = 10;
        float MAXTHUMB = 32767;
        float MINTHUMB = 32768;
        float MAXTRIGGER = 255;
        float MINNECK = 565;
        float ratio;
        string mood = "Neutral";
        int cycle = 0;

        float headPositionX;
        float headPositionY;
        float neckPosition;

        //button states
        bool xButtonPressed = false;
        bool xButtonReleased =  false;
        bool xButtonHeld = false;

        bool yButtonPressed = false;
        bool yButtonReleased = false;
        bool yButtonHeld = false;

        bool aButtonPressed = false;
        bool aButtonReleased = false;
        bool aButtonHeld = false;

        bool bButtonPressed = false;
        bool bButtonReleased = false;
        bool bButtonHeld = false;

        bool upButtonPressed = false;
        bool upButtonReleased = false;
        bool upButtonHeld = false;

        bool downButtonPressed = false;
        bool downButtonReleased = false;
        bool downButtonHeld = false;

        bool leftButtonPressed = false;
        bool leftButtonReleased = false;
        bool leftButtonHeld = false;

        bool rightButtonPressed = false;
        bool rightButtonReleased = false;
        bool rightButtonHeld = false;

        bool rightStickPressed = false;
        bool rightStickHeld = false;

        bool leftStickPressed = false;
        bool leftStickHeld = false;

        bool backPressed = false;
        bool backHeld = false;

        public MainWindow()
        {
            InitializeComponent();
            // Get 1st controller available

            if (!controller.IsConnected)
            {
                Console.WriteLine("Controller not found");
                return;
            }

            if (controller == null)
            {
                Console.WriteLine("No XInput controller installed");
            }
            else
            {

                Console.WriteLine("Found a XInput controller available");
                Console.WriteLine("Press buttons on the controller to display events or escape key to exit... ");
                headPositionX = 0.0f;
                headPositionY = 0.0f;
                neckPosition = 0.0f;
            }

            AiboConnection.HOST_IP = "172.17.100.12";
            AiboConnection.ConnectToAIBO();

            updateImageTimer.Tick += new EventHandler(updateImageTimer_Tick);
            updateImageTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);  // choose the frame rate that works best for your project
            updateImageTimer.Start();

            updateControllerTimer.Tick += new EventHandler(updateControllerTimer_Tick);
            updateControllerTimer.Interval = new TimeSpan(0, 0, 0, 0, 150);  // controller rate
            updateControllerTimer.Start();

            batteryTimer.Tick += new EventHandler(battery_Thread);
            batteryTimer.Interval = new TimeSpan(0, 0, 15);
            batteryTimer.Start();

            ChildWarning.Visibility = Visibility.Hidden;

            dpadImageVisibility(false);

            childtimer = new System.Timers.Timer();
            childtimer.Interval = 60000;
            childtimer.Elapsed += new ElapsedEventHandler(childtimer_Elapsed);
            childtimer.Start();

            initMood();
            // I didn't choose a good size for my image. Make sure you're careful about that.

        }

        private void dpadImageVisibility(bool on)
        {
            if (on)
            {
                DpadImage.Visibility = Visibility.Visible;
                DpadTextDown.Visibility = Visibility.Visible;
                DpadTextUp.Visibility = Visibility.Visible;
                DpadTextLeft.Visibility = Visibility.Visible;
                DpadTextRight.Visibility = Visibility.Visible;
            }
            else
            {
                DpadImage.Visibility = Visibility.Hidden;
                DpadTextDown.Visibility = Visibility.Hidden;
                DpadTextUp.Visibility = Visibility.Hidden;
                DpadTextLeft.Visibility = Visibility.Hidden;
                DpadTextRight.Visibility = Visibility.Hidden;
            }

        }

        private void updateButtonStates()
        {
            controllerState = controller.GetState();

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.X) != 0)
            {
                if (!xButtonPressed)
                {
                    xButtonPressed = true;
                }
                else
                {
                    xButtonHeld = true;
                }
            }
            else if (xButtonPressed)
            {
                xButtonPressed = false;
                xButtonHeld = false;
                xButtonReleased = true;
            }
            else
            {
                xButtonPressed = false;
                xButtonHeld = false;
                xButtonReleased = false;
            }

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.Y) != 0)
            {
                if (!yButtonPressed)
                {
                    yButtonPressed = true;
                }
                else
                {
                    yButtonHeld = true;
                }
            }
            else if (yButtonPressed)
            {
                yButtonPressed = false;
                yButtonHeld = false;
                yButtonReleased = true;
            }
            else
            {
                yButtonPressed = false;
                yButtonHeld = false;
                yButtonReleased = false;
            }

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.A) != 0)
            {
                if (!aButtonPressed)
                {
                    aButtonPressed = true;
                }
                else
                {
                    aButtonHeld = true;
                }
            }
            else if (aButtonPressed)
            {
                aButtonPressed = false;
                aButtonHeld = false;
                aButtonReleased = true;
            }
            else
            {
                aButtonPressed = false;
                aButtonHeld = false;
                aButtonReleased = false;
            }

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.B) != 0)
            {
                if (!bButtonPressed)
                {
                    bButtonPressed = true;
                }
                else
                {
                    bButtonHeld = true;
                }
            }
            else if (bButtonPressed)
            {
                bButtonPressed = false;
                bButtonHeld = false;
                bButtonReleased = true;
            }
            else
            {
                bButtonPressed = false;
                bButtonHeld = false;
                bButtonReleased = false;
            }

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.DPadUp) != 0)
            {
                if (!upButtonPressed)
                {
                    upButtonPressed = true;
                }
                else
                {
                    upButtonHeld = true;
                }
            }
            else if (upButtonPressed)
            {
                upButtonPressed = false;
                upButtonHeld = false;
                upButtonReleased = true;
            }
            else
            {
                upButtonPressed = false;
                upButtonHeld = false;
                upButtonReleased = false;
            }

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.DPadDown) != 0)
            {
                if (!downButtonPressed)
                {
                    downButtonPressed = true;
                }
                else
                {
                    downButtonHeld = true;
                }
            }
            else if (downButtonPressed)
            {
                downButtonPressed = false;
                downButtonHeld = false;
                downButtonReleased = true;
            }
            else
            {
                downButtonPressed = false;
                downButtonHeld = false;
                downButtonReleased = false;
            }

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.DPadLeft) != 0)
            {
                if (!leftButtonPressed)
                {
                    leftButtonPressed = true;
                }
                else
                {
                    leftButtonHeld = true;
                }
            }
            else if (leftButtonPressed)
            {
                leftButtonPressed = false;
                leftButtonHeld = false;
                leftButtonReleased = true;
            }
            else
            {
                leftButtonPressed = false;
                leftButtonHeld = false;
                leftButtonReleased = false;
            }

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.DPadRight) != 0)
            {
                if (!rightButtonPressed)
                {
                    rightButtonPressed = true;
                }
                else
                {
                    rightButtonHeld = true;
                }
            }
            else if (rightButtonPressed)
            {
                rightButtonPressed = false;
                rightButtonHeld = false;
                rightButtonReleased = true;
            }
            else
            {
                rightButtonPressed = false;
                rightButtonHeld = false;
                rightButtonReleased = false;
            }

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.RightThumb) != 0)
            {
                if (!rightStickPressed)
                {
                    rightStickPressed = true;
                }
                else
                {
                    rightStickHeld = true;
                }
            }
            else if (rightStickPressed)
            {
                rightStickPressed = false;
                rightStickHeld = false;
            }

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.LeftThumb) != 0)
            {
                if (!leftStickPressed)
                {
                    leftStickPressed = true;
                }
                else
                {
                    leftStickHeld = true;
                }
            }
            else if (leftStickPressed)
            {
                leftStickPressed = false;
                leftStickHeld = false;
            }

            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.Back) != 0)
            {
                if (!backPressed)
                {
                    backPressed = true;
                }
                else
                {
                    backHeld = true;
                }
            }
            else if (backPressed)
            {
                backPressed = false;
                backHeld = false;
            }
        }

        private void childtimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                ChildWarning.Visibility = Visibility.Visible;
                Yield.Visibility = Visibility.Visible;
            }));
            
            childmessagetimer = new System.Timers.Timer();
            childmessagetimer.Interval = 4000;
            childmessagetimer.Elapsed += new ElapsedEventHandler(childmessagetimer_Elapsed);
            childmessagetimer.Start();
            
        }

        private void childmessagetimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                ChildWarning.Visibility = Visibility.Hidden;
                Yield.Visibility = Visibility.Hidden;
            }));
            
            childmessagetimer.Stop();
        }

        void updateControllerTimer_Tick(object sender, EventArgs e)
        {
            updateButtonStates();
            if (AiboConnection.WalkConnected)
                aiboMovement();
        }

        void updateImageTimer_Tick(object sender, EventArgs e)
        {
            if (AiboConnection.CurrImage != null)
            {
                // These are only to convert the image to WPF image!! 
                // if you're using windows forms (which I don't recommend) all you have to do is to say:
                // aiboCameraImage.Image = AiboConnection.CurrImage;
                MemoryStream ms = new MemoryStream();
                AiboConnection.CurrImage.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                ms.Position = 0;
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.EndInit();
                aiboCameraImage.Source = bi;
            }
            //if (AiboConnection.WalkConnected)
            //    aiboMovement();
            if (AiboConnection.batteryInfo != null)
            {
                //Console.WriteLine(AiboConnection.batteryInfo);
                Battery_Percentage.Text = AiboConnection.batteryInfo;
            }

            updateMusicToggle();

        }

        void changeMood()
        {
            if(mood.Equals("Happy"))
                Mood.Source = new BitmapImage(new Uri(@"pack://application:,,,/Happy.png"));
            else if (mood.Equals("Angry"))
                Mood.Source = new BitmapImage(new Uri(@"pack://application:,,,/Angry.png"));
            else if (mood.Equals("Sad"))
                Mood.Source = new BitmapImage(new Uri(@"pack://application:,,,/Sad.png"));
            else if (mood.Equals("Neutral"))
                Mood.Source = new BitmapImage(new Uri(@"pack://application:,,,/Neutral.png"));
        }

        void rotateCameraImage(double angle)
        {
            RotateTransform rotateImage = (RotateTransform)aiboCameraImage.RenderTransform;
            DoubleAnimation cameraRotationAnimation = new DoubleAnimation();
            cameraRotationAnimation.From = rotateImage.Angle;
            cameraRotationAnimation.To = angle;
            cameraRotationAnimation.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300));
            //headAnimation.RepeatBehavior = new RepeatBehavior(4);
            double a = AiboHead.Margin.Bottom;

            rotateImage.BeginAnimation(RotateTransform.AngleProperty, cameraRotationAnimation);
        }

        void rotateHeadImage(double angle)
        {
            RotateTransform rotateImage = (RotateTransform)AiboHead.RenderTransform;
            DoubleAnimation headAnimation = new DoubleAnimation();
            headAnimation.From = rotateImage.Angle;
            headAnimation.To = angle;
            headAnimation.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300)); 
            //headAnimation.RepeatBehavior = new RepeatBehavior(4);
            double a = AiboHead.Margin.Bottom;

            rotateImage.BeginAnimation(RotateTransform.AngleProperty, headAnimation);
        }

        void movePointer()
        {
            TranslateTransform trans = (TranslateTransform)Pointer.RenderTransform;
            DoubleAnimation movePointer = new DoubleAnimation(trans.Y, MINNECK * -neckPosition, TimeSpan.FromMilliseconds(100));
            trans.BeginAnimation(TranslateTransform.YProperty, movePointer);
        }

        private void aiboMovement()
        {
            controllerState = controller.GetState();

            //Basic Movement//////////////////////////////

            //check for return to idle state or clear previous movement
            //Only check if it was moving
            if (legMoving)
            {
                if ((controllerState.Gamepad.LeftThumbY < DEADZONE
                        && controllerState.Gamepad.LeftThumbY > -DEADZONE))
                {
                    AiboConnection.SendWalkCmd('s', 0.0f);                  //clear strafe
                    if ((controllerState.Gamepad.LeftThumbX < DEADZONE
                        && controllerState.Gamepad.LeftThumbX > -DEADZONE))
                    {
                        AiboConnection.SendWalkCmd('f', 0.0f);
                        AiboConnection.SendWalkCmd('s', 0.0f);
                        legMoving = false;                                  //only clear if both are idle
                    }
                }
                else if ((controllerState.Gamepad.LeftThumbX < DEADZONE
                        && controllerState.Gamepad.LeftThumbX > -DEADZONE))
                    AiboConnection.SendWalkCmd('f', 0.0f);
                if(((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.LeftShoulder) == 0)
                        && (controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.RightShoulder) == 0)
                    AiboConnection.SendWalkCmd('r', 0.0f);
            }
            //Up, down
            if ((controllerState.Gamepad.LeftThumbY > DEADZONE))
            {
                AiboConnection.SendWalkCmd('f', 1.0f);      
                legMoving = true;
            }
            else if ((controllerState.Gamepad.LeftThumbY < -DEADZONE))
            {
                AiboConnection.SendWalkCmd('f', -1.0f);      
                legMoving = true;
            }
            //Left, Right
            if ((controllerState.Gamepad.LeftThumbX > DEADZONE))
            {
                AiboConnection.SendWalkCmd('s', -1.0f);
                legMoving = true;
            }
            else if ((controllerState.Gamepad.LeftThumbX < -DEADZONE))
            {
                AiboConnection.SendWalkCmd('s', 1.0f);
                legMoving = true;
            }
            //POWERWALK TOGGLE
            if (leftStickPressed && !leftStickHeld)
            {
                
                AiboConnection.InitRemoteWalkControl(true);


                pWalk = !pWalk;
                if (pWalk == true)
                {
                    PowerWalkRed.Visibility = Visibility.Hidden;
                    
                }
                else
                {
                    PowerWalkRed.Visibility = Visibility.Visible;
                    
                }
            }
            //Head movement///////////////////////////////
            //up, down
            if ((controllerState.Gamepad.RightThumbY > DEADZONE))
            {
                ratio = (float)controllerState.Gamepad.RightThumbY / MAXTHUMB;
                headPositionY += ratio;
                if (headPositionY >= 1.0f)
                    headPositionY = 1.0f;
                AiboConnection.SendHeadPointCmd('r', headPositionY);
            }
            else if ((controllerState.Gamepad.RightThumbY < -DEADZONE))
            {
                ratio = (float)controllerState.Gamepad.RightThumbY / MINTHUMB;
                headPositionY += ratio;
                if (headPositionY <= -1.0f)
                    headPositionY = -1.0f;
                AiboConnection.SendHeadPointCmd('r', headPositionY);
            }
            //left, right
            if ((controllerState.Gamepad.RightThumbX < -DEADZONE))
            {
                ratio = ((float)controllerState.Gamepad.RightThumbX / -MINTHUMB) / 2;
                headPositionX += ratio;
                if (headPositionX >= 1.0f)
                    headPositionX = 1.0f;
                rotateHeadImage(-headPositionX * 90);
                AiboConnection.SendHeadPointCmd('p', headPositionX);
            }
            else if ((controllerState.Gamepad.RightThumbX > DEADZONE))
            {
                ratio = ((float)controllerState.Gamepad.RightThumbX / -MAXTHUMB) / 2;
                headPositionX += ratio;
                if (headPositionX <= -1.0f)
                    headPositionX = -1.0f;
                rotateHeadImage(-headPositionX * 90);
                AiboConnection.SendHeadPointCmd('p', headPositionX);
            }

           
            //rotate left, right
            if ((controllerState.Gamepad.RightTrigger > TRIGGER_DEADZONE))
            {
                ratio = (float)controllerState.Gamepad.RightTrigger / MAXTRIGGER;               
                AiboConnection.SendWalkCmd('r', -ratio);
                legMoving = true;
            }
            if ((controllerState.Gamepad.LeftTrigger > TRIGGER_DEADZONE))
            {
                ratio = (float)controllerState.Gamepad.LeftTrigger / -MAXTRIGGER;
                AiboConnection.SendWalkCmd('r', -ratio);
                legMoving = true;
            }

            //Neck movement///////////////////////////
            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.LeftShoulder) != 0)
            {
                neckPosition -= 0.1f;
                if (neckPosition <= -1.0f)
                {
                    neckPosition = -1.0f;
                }
                AiboConnection.SendHeadPointCmd('t', neckPosition);
                movePointer();
            }
            else if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.RightShoulder) != 0)
            {
                neckPosition += 0.1f;
                if (neckPosition >= 0.0f)
                    neckPosition = 0.0f;
                AiboConnection.SendHeadPointCmd('t', neckPosition);
                movePointer();
            }
            //Buttons/////////////////////////
            if ((controller.GetState().Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.Start) != 0)
            {
                //Get-up
                
                timer = new System.Timers.Timer();
                timer.Interval = 3000;
                timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                timer.Start();
                AiboConnection.WalkConnected = false;
                AiboConnection.Getup();
            }
            if (backPressed && !backHeld)
            {
                // Play Music
                AiboConnection.WalkConnected = false;
                timer = new System.Timers.Timer();
                timer.Interval = 4000;
                timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                timer.Start();
                timer2 = new System.Timers.Timer();
                timer2.Interval = 155000;
                timer2.Elapsed += new ElapsedEventHandler(timer2_Elapsed);
                timer2.Start();
                AiboConnection.Music();
                musictoggle = !musictoggle;
            }
            if (rightStickPressed && !rightStickHeld)
            {
                //Reset head position
                headPositionX = 0.0f;
                headPositionY = 0.0f;
                neckPosition = 0.0f;
                rotateHeadImage(headPositionX * 90);
                movePointer();
                // Send to Aibo
                AiboConnection.SendHeadPointCmd('t', neckPosition);
                AiboConnection.SendHeadPointCmd('p', headPositionX);
                AiboConnection.SendHeadPointCmd('r', headPositionY);
            }
            if (bButtonPressed && !bButtonHeld)
            {
                if (!lookingBack)
                {
                    //Look back to child
                    headPositionX = 1.0f;
                    headPositionY = 0.0f;
                    neckPosition = 0.0f;
                    movePointer();
                    rotateHeadImage(-headPositionX * 90);
                    //Send to Aibo
                    AiboConnection.SendHeadPointCmd('t', neckPosition);
                    AiboConnection.SendHeadPointCmd('p', headPositionX);
                    AiboConnection.SendHeadPointCmd('r', headPositionY);
                    //Rotate Aibo
                    if(pWalk)
                        AiboConnection.SendWalkCmd('r', 0.05f);
                    else
                        AiboConnection.SendWalkCmd('r', 0.22f);
                    lookingBack = true;

                    childtimer.Stop();
                    childtimer.Start();
                    LookBack.Visibility = Visibility.Visible;
                }
                else
                {
                    //Reposition to forward
                    headPositionX = 0.0f;
                    headPositionY = 0.0f;
                    neckPosition = 0.0f;
                    movePointer();
                    rotateHeadImage(-headPositionX * 90);
                    //Send to Aibo
                    AiboConnection.SendHeadPointCmd('t', neckPosition);
                    AiboConnection.SendHeadPointCmd('p', headPositionX);
                    AiboConnection.SendHeadPointCmd('r', headPositionY);
                    //Rotate Aibo
                    if(pWalk)
                        AiboConnection.SendWalkCmd('r', -0.05f);
                    else
                        AiboConnection.SendWalkCmd('r', -0.22f);
                    lookingBack = false;
                    LookBack.Visibility = Visibility.Hidden;
                }
            }

            if (aButtonHeld)
            {
                dpadImageVisibility(true);
                if (upButtonPressed && !upButtonHeld)
                {
                    if (musictoggle == true)
                        AiboConnection.Music();
                    musictoggle = false;

                    AiboConnection.Dialogue(mood, "Up");                    
                }

                if (leftButtonPressed && !leftButtonHeld)
                {
                    if (musictoggle == true)
                        AiboConnection.Music();
                    musictoggle = false;
                    AiboConnection.Dialogue(mood, "Left"); 
                }

                if (downButtonPressed && !downButtonHeld)
                {
                    if (musictoggle == true)
                         AiboConnection.Music();
                    musictoggle = false;
                    AiboConnection.Dialogue(mood, "Down"); 
                }

                if (rightButtonPressed && !rightButtonHeld)
                {
                    if (musictoggle == true)
                        AiboConnection.Music();
                    musictoggle = false;
                    AiboConnection.Dialogue(mood, "Right"); 
                }
            }
            else if (aButtonReleased)
            {
                dpadImageVisibility(false);
            }

            if (xButtonPressed && !xButtonHeld)
            {
                AiboConnection.SendWalkCmd('9', 0.0f);
            }

            if (yButtonPressed && !yButtonHeld)
            {
                if (mood.Equals("Happy") || mood.Equals("Sad") || mood.Equals("Angry"))
                {
                    GestureInProgress.Visibility = Visibility.Visible;
                }
                //Gestures
                if (cycle % 3 == 0 && mood.Equals("Happy"))
                {
                    needGetup = true;
                    timer = new System.Timers.Timer();
                    timer.Interval = 6500;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);                   
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Dance();
                    cycle++;
                        
                }                   
            
                else if (cycle % 3 == 1 && mood.Equals("Happy"))
                {
                    timer = new System.Timers.Timer();
                    timer.Interval = 7000;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Grab();
                    cycle++;
                        
                }

                else if (cycle % 3 == 2 && mood.Equals("Happy"))
                {
                    needGetup = true;
                    timer = new System.Timers.Timer();
                    timer.Interval = 4000;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Sit();
                    cycle++;
                }

                else if (cycle % 2 == 0 && mood.Equals("Angry"))
                {
                    timer = new System.Timers.Timer();
                    timer.Interval = 4000;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Roar();
                    AiboConnection.SendWalkCmd('8', 0.0f);  // Growl
                    cycle++;
                }


                else if (cycle % 2 == 1 && mood.Equals("Angry"))
                {
                    timer = new System.Timers.Timer();
                    timer.Interval = 4000;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Crouch();
                    AiboConnection.SendWalkCmd('8', 0.0f);  // Growl
                    cycle++;
                }

                else if (cycle % 2 == 0 && mood.Equals("Sad"))
                {
                    timer = new System.Timers.Timer();
                    timer.Interval = 4000;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Liedown();
                    AiboConnection.SendWalkCmd('7', 0.0f);  // Whimper
                    cycle++;
                }

                else if (cycle % 2 == 1 && mood.Equals("Sad"))
                {
                    timer = new System.Timers.Timer();
                    timer.Interval = 4000;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Sprawl();
                    AiboConnection.SendWalkCmd('7', 0.0f);  // Whimper
                    cycle++;
                }

                
            }

            //DPAD//////////////////////////
            //Ignored if a button held 
            //Happy
            if (!aButtonPressed)
            {
                
                if (upButtonPressed && !upButtonHeld)
                {
                    //AiboConnection.SendHeadPointCmd('0', 0.0f);
                    AiboConnection.SendHeadPointCmd('1', 0.0f);
                    AiboConnection.SendWalkCmd('6', 0.0f);      // Yap
                    mood = "Happy";
                    if (!needResetTail)
                        AiboConnection.Wagtail();
                    needResetTail = true;
                    needGetup = true;
                    GestureInProgress.Visibility = Visibility.Visible;
                    timer = new System.Timers.Timer();
                    timer.Interval = 7000;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Sit();
                    changeMood();

                    DpadTextUp.Text = Happy1;
                    DpadTextDown.Text = Happy2;
                    DpadTextLeft.Text = "";
                    DpadTextRight.Text = "";
                }
                //Sad
                if (leftButtonPressed && !leftButtonHeld)
                {
                    AiboConnection.SendHeadPointCmd('2', 0.0f);
                    AiboConnection.SendWalkCmd('7', 0.0f);      // Whimper
                    mood = "Sad";
                    if (needResetTail == true)
                    {
                        AiboConnection.Wagtail();
                        needResetTail = false;
                    }
                    GestureInProgress.Visibility = Visibility.Visible;
                    timer = new System.Timers.Timer();
                    timer.Interval = 4000;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Liedown();
                    changeMood();

                    DpadTextUp.Text = Sad1;
                    DpadTextDown.Text = Sad2;
                    DpadTextLeft.Text = Sad3;
                    DpadTextRight.Text = Sad4;
                }
                //Angry
                if (downButtonPressed && !downButtonHeld)
                {
                    AiboConnection.SendHeadPointCmd('0', 0.0f);
                    AiboConnection.SendWalkCmd('8', 0.0f);      // Growl
                    mood = "Angry";
                    if (needResetTail == true)
                    {
                        AiboConnection.Wagtail();
                        needResetTail = false;
                    }
                    GestureInProgress.Visibility = Visibility.Visible;
                    timer = new System.Timers.Timer();
                    timer.Interval = 4000;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Roar();
                    changeMood();

                    DpadTextUp.Text = Angry1;
                    DpadTextDown.Text = Angry2;
                    DpadTextLeft.Text = Angry3;
                    DpadTextRight.Text = "";
                }
                //Neutral
                if (rightButtonPressed && !rightButtonHeld)
                {
                    AiboConnection.SendHeadPointCmd('3', 0.0f);
                    mood = "Neutral";
                    if (needResetTail == true)
                    {
                        AiboConnection.Wagtail();
                        needResetTail = false;
                    }
                    GestureInProgress.Visibility = Visibility.Visible;
                    timer = new System.Timers.Timer();
                    timer.Interval = 2000;
                    timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                    timer.Start();
                    AiboConnection.WalkConnected = false;
                    AiboConnection.Pounce();
                    changeMood();

                    DpadTextUp.Text = "";
                    DpadTextDown.Text = "";
                    DpadTextLeft.Text = "";
                    DpadTextRight.Text = "";
                }
            }
        }


        public void initMood()
        {
            DpadTextUp.Text = "";
            DpadTextDown.Text = "";
            DpadTextLeft.Text = "";
            DpadTextRight.Text = "";
        }

        public void battery_Thread(object sender, EventArgs e)
        {
            //Console.WriteLine(AiboConnection.batteryInfo);
            Battery_Percentage.Text = AiboConnection.batteryInfo;
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            if (needGetup)
                AiboConnection.Getup();
            AiboConnection.WalkConnected = true;
            needGetup = false;
            this.Dispatcher.Invoke((Action)(() =>
            {
                GestureInProgress.Visibility = Visibility.Hidden;
            }));
        }
        private void timer2_Elapsed(object sencder, ElapsedEventArgs e)
        {
            if (musictoggle == true) 
                this.Dispatcher.Invoke((Action)(() =>
                 {
                   MusicRed.Visibility = Visibility.Visible;
                   }));
                
        }

        private void onClose(object sender, System.ComponentModel.CancelEventArgs e)
        {
            AiboConnection.CloseConnection();
        }

        void updateMusicToggle()
        {
            if (musictoggle == false)
            {
                MusicRed.Visibility = Visibility.Visible;
            }
            else
            {
                MusicRed.Visibility = Visibility.Hidden;
            }
        }

    }
}