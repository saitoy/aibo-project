#include "StartupBehavior.h"

#include "Behaviors/Controls/ControlBase.h"
#include "Behaviors/Controls/BehaviorSwitchControl.h"

#include "Behaviors/Mon/ViewWMVarsBehavior.h"
#include "Behaviors/Mon/WalkControllerBehavior.h"
#include "Behaviors/Mon/UPennWalkControllerBehavior.h"
#include "Behaviors/Mon/HeadPointControllerBehavior.h"
#include "Behaviors/Mon/StewartPlatformBehavior.h"
#include "Behaviors/Mon/Aibo3DControllerBehavior.h"
#include "Behaviors/Mon/EStopControllerBehavior.h"
#include "Behaviors/Mon/WMMonitorBehavior.h"
#include "Behaviors/Mon/RawCamBehavior.h"
#include "Behaviors/Mon/SegCamBehavior.h"
#include "Behaviors/Mon/RegionCamBehavior.h"
#include "Behaviors/Mon/WorldStateSerializerBehavior.h"
#include "Behaviors/Mon/MicrophoneServer.h"
#include "Behaviors/Mon/SpeakerServer.h"
#include "Behaviors/Mon/EchoBehavior.h"
#include "Behaviors/Mon/SpiderMachineBehavior.h"

ControlBase*
StartupBehavior::SetupTekkotsuMon() {
	addItem(new ControlBase("TekkotsuMon","Servers for GUIs"));
	startSubMenu();
	{ 
		addItem((new BehaviorSwitchControl<HeadPointControllerBehavior>("Head Remote Control",false)));
		addItem((new BehaviorSwitchControl<WalkControllerBehavior>("Walk Remote Control",false)));
		addItem((new BehaviorSwitchControl<UPennWalkControllerBehavior>("UPenn Walk Remote Control",false)));
		addItem((new BehaviorSwitchControl<ViewWMVarsBehavior>("View WMVars",false)));
		addItem((new BehaviorSwitchControl<WMMonitorBehavior>("Watchable Memory Monitor",false))->start());
		addItem((new BehaviorSwitchControl<StewartPlatformBehavior>("Stewart Platform",false)));
		addItem((new BehaviorSwitchControl<Aibo3DControllerBehavior>("Aibo 3D",false)));
		BehaviorSwitchControlBase * wss=NULL;
		addItem((wss=new BehaviorSwitchControl<WorldStateSerializerBehavior>("World State Serializer",false)));
		Aibo3DControllerBehavior::setSerializerControl(wss);
		addItem((new BehaviorSwitchControl<RawCamBehavior>("Raw Cam Server",false)));
		addItem((new BehaviorSwitchControl<SegCamBehavior>("Seg Cam Server",false)));
		addItem((new BehaviorSwitchControl<RegionCamBehavior>("Region Cam Server",false)));
		addItem((new BehaviorSwitchControlBase(new EStopControllerBehavior(stop_id)))->start());
		addItem(new BehaviorSwitchControlBase(MicrophoneServer::GetInstance()));
		addItem(new BehaviorSwitchControlBase(SpeakerServer::GetInstance()));
		addItem((new BehaviorSwitchControl<EchoBehavior>("Echo Client/Server",false)));
		addItem((new BehaviorSwitchControl<SpiderMachineBehavior>("Spider State Machines Server",false)));
	}
	return endSubMenu();
}
