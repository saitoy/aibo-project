#include "StartupBehavior.h"

#include "Behaviors/Controls/ControlBase.h"
#include "Behaviors/Controls/BehaviorSwitchControl.h"

#include "Behaviors/Services/FlashIPAddrBehavior.h"
#include "Behaviors/Demos/SimpleChaseBallBehavior.h"
#include "Behaviors/Demos/StareAtBallBehavior.h"
#include "Behaviors/Services/AutoGetupBehavior.h"
#include "Behaviors/Services/BatteryMonitorBehavior.h"
#include "Behaviors/Demos/HeadLevelBehavior.h"
#include "Behaviors/Demos/ToggleHeadLightBehavior.h"
#include "Behaviors/Demos/CrashTestBehavior.h"
#include "Behaviors/Demos/FreezeTestBehavior.h"
#include "Behaviors/Demos/RelaxBehavior.h"
#include "Behaviors/Services/WorldStateVelDaemon.h"
#include "Behaviors/Demos/CameraBehavior.h"
#include "Behaviors/Demos/MotionStressTestBehavior.h"
#include "Behaviors/Demos/ASCIIVisionBehavior.h"
#include "Behaviors/Demos/TstBehavior.h"
#include "Behaviors/Nodes/TailWagNode.h"
#include "Sound/PitchDetector.h"
#include "Behaviors/Demos/HelloWorldBehavior.h"
#include "Behaviors/Services/FlashIPAddrBehavior.h"
#include "Behaviors/Demos/TestBehaviors.h"

ControlBase*
StartupBehavior::SetupBackgroundBehaviors() {
	addItem(new ControlBase("Background Behaviors","Background daemons and monitors"));
	startSubMenu();
	{ 
		addItem(new BehaviorSwitchControl<TstBehavior>("TstBehavior", false));
		addItem(new BehaviorSwitchControl<HelloWorldBehavior>("HelloWorldBehavior", false));
		addItem(new BehaviorSwitchControl<SimpleChaseBallBehavior>("Simple Chase Ball",false));
		addItem(new BehaviorSwitchControl<StareAtBallBehavior>("Stare at Ball",false));
		addItem(new BehaviorSwitchControl<HeadLevelBehavior>("Head Level",false));
		if(state->robotDesign & WorldState::ERS220Mask)
			addItem(new BehaviorSwitchControl<ToggleHeadLightBehavior>("Toggle Head Light",false));
		addItem(new BehaviorSwitchControl<TailWagNode>("Wag Tail",false));
		addItem(new BehaviorSwitchControl<RelaxBehavior>("Relax",false));
		addItem(new BehaviorSwitchControl<CameraBehavior>("Camera",false));
		addItem((new BehaviorSwitchControl<ASCIIVisionBehavior>("ASCIIVision",false)));
		addItem(new ControlBase("Debugging Tests","Stress tests"));
		startSubMenu();
		{
			addItem(new BehaviorSwitchControl<MotionStressTestBehavior>("Motion Stress Test",false));
			addItem(new BehaviorSwitchControl<CrashTestBehavior>("Crash Test",false));
			addItem(new BehaviorSwitchControl<FreezeTestBehavior>("Freeze Test",false));
			// these following behaviors are all found in TestBehaviors.h, too small to warrant individual files
			addItem(new BehaviorSwitchControl<InstantMotionTestBehavior>("Instant MC Add/Remove",false));
			addItem(new BehaviorSwitchControl<BusyLoopTestBehavior>("Busy Loop Behavior",false));
			addItem(new BehaviorSwitchControl<BusyMCTestBehavior>("Busy Loop MC",false));
			addItem(new BehaviorSwitchControl<SuicidalBehavior>("Suicidal Behavior",false));
			addItem(new BehaviorSwitchControl<EchoTextBehavior>("Echo Text",false));
			addItem(new BehaviorSwitchControl<SaveImagePyramidBehavior>("Save Image Pyramid",false));
		}
		endSubMenu();
		addItem(new ControlBase("System Daemons","Provide some common sensor or event processing"));
		startSubMenu();
		{
			addItem((new BehaviorSwitchControl<AutoGetupBehavior>("Auto Getup",false)));
			addItem((new BehaviorSwitchControl<FlashIPAddrBehavior>("Flash IP Address",false))->start());
			addItem((new BehaviorSwitchControl<WorldStateVelDaemon>("World State Vel Daemon",false))->start());
			addItem((new BehaviorSwitchControl<BatteryMonitorBehavior>("Battery Monitor",false))->start());
			addItem((new BehaviorSwitchControl<PitchDetector>("Pitch Detection",false))->start());
		}
		endSubMenu();
	}
	return endSubMenu();
}
