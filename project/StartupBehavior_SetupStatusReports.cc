#include "StartupBehavior.h"

#include "Behaviors/Controls/ControlBase.h"

#include "Behaviors/Controls/BatteryCheckControl.h"
#include "Behaviors/Controls/FreeMemReportControl.h"
#include "Behaviors/Controls/ProfilerCheckControl.h"
#include "Behaviors/Controls/EventLogger.h"
#include "Behaviors/Controls/SensorObserverControl.h"
#include "Behaviors/Controls/BehaviorReportControl.h"
#include "Behaviors/Controls/NetworkStatusControl.h"

ControlBase*
StartupBehavior::SetupStatusReports() {
	addItem(new ControlBase("Status Reports","Displays information about the runtime environment on the console"));
	startSubMenu();
	{
		addItem(new BehaviorReportControl());
		addItem(new BatteryCheckControl());
		addItem(new ProfilerCheckControl());
		addItem(new EventLogger());
		addItem(new SensorObserverControl());
		FreeMemReportControl * tmp=new FreeMemReportControl();
		tmp->DoStart();
		addItem(tmp);
		addItem(new NetworkStatusControl());
	}
	return endSubMenu();
}
